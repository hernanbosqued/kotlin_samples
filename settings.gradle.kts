rootProject.name = "samples"

include(
    "library",
    "hello",
    "coroutines",
    "extensions",
    "template",
    "operators",
    "infix",
    "inline",
    "reified",
    "sealed",
    "lambda",
    "delegates"
)
